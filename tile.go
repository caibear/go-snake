package main

import (
    "fmt"
)

type Tile struct {
    x, y  int
    owner TileOwner
    vao   *Vao
}

type TileOwner interface {
    GetTile()      *Tile
    IsEmpty()      bool
    move(x, y int) (int, int)
    clear()
}

type EmptyOwner struct {

}

func (*EmptyOwner) GetTile() *Tile {
    return nil
}

func (*EmptyOwner) IsEmpty() bool {
    return false
}

func (*EmptyOwner) move(x, y int) (int, int) {
    return x, y
}

func (*EmptyOwner) clear() {

}

func newTile(x, y int) *Tile {
    tile := &Tile{x: x, y: y}
    tile.createVao(x, y, boardWidth, boardHeight, 0)
    window.addDrawable(tile)
    return tile
}

func newVisualTile(x, y int) *Tile {
    tile := &Tile{x: x, y: y}
    tile.createVao(x, y, boardWidth, boardHeight, 1)
    window.addDrawable(tile)
    return tile
}

func (tile *Tile) setOwner(owner TileOwner) {
    if owner != nil {
        tile.vao.visible = true
    } else {
        tile.vao.visible = false
    }
    tile.owner = owner
}

func (tile *Tile) getVao() *Vao {
    return tile.vao
}

func (tile *Tile) GetPos() (int, int) {
    if tile == nil {
        return -1, -1
    }
    return tile.x, tile.y
}

func (tile *Tile) GetOwner() TileOwner {
    return tile.owner
}

func (tile *Tile) GetRelative(direction Direction) *Tile {
    dx, dy := direction.getDelta()
    return board.Get(tile.x + dx, tile.y + dy)
}

func (tile *Tile) EmptyRelative(direction Direction) bool {
    dx, dy := direction.getDelta()
    return board.IsEmpty(tile.x + dx, tile.y + dy)
}

func (tile *Tile) Direction(t *Tile) Direction {
    return fromDelta(t.x - tile.x, t.y - tile.y)
}

func (tile *Tile) DistanceTo(t *Tile) int {
    return Distance2(tile.x, tile.y, t.x, t.y)
}

func (tile *Tile) IsEmpty() bool {
    return tile.owner == nil || tile.owner.IsEmpty()
}

func (tile *Tile) createVao(x, y, width, height, style int) {
    var mesh []float32
    if style == 0 {
        mesh = square
    } else {
        mesh = triangle
    }
    vertices := make([]float32, len(mesh), len(mesh))
    copy(vertices, mesh)

    for i := 0; i < len(vertices); i++ {
        var position float32
        var size float32
        switch i % 3 {
        case 0:
            size = 1.0 / float32(width)
            position = float32(x) * size
        case 1:
            size = 1.0 / float32(height)
            position = float32(y) * size
        default:
            continue
        }

        if vertices[i] < 0 {
            vertices[i] = (position * 2) - 1
        } else {
            vertices[i] = ((position + size) * 2) - 1
        }
    }

    tile.vao = createVao(vertices)
    tile.vao.visible = false
}

func (tile *Tile) String() string {
    return fmt.Sprint("(", tile.x, tile.y, ")")
}