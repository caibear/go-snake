package main

import "math/rand"

type Direction int

const (
    zero Direction = iota
    up
    down
    left
    right
)

var directionValues = [4]Direction{up, down, left, right}

func randomDirection(rand *rand.Rand) Direction {
    i := rand.Int63()
    return Direction(i % 4 + 1)
}

func (direction Direction) xParallel() bool {
    switch direction {
    case right:
        fallthrough
    case left:
        return true
    default:
        return false
    }
}

func (direction Direction) getDelta() (int, int) {
    switch direction {
    case up:
        return 0, 1
    case down:
        return 0, -1
    case left:
        return -1, 0
    case right:
        return 1, 0
    case zero:
        return 0, 0
    }
    panic("Invalid direction")
}

func fromDelta(x, y int) Direction {
    switch x {
    case 1:
        return right
    case -1:
        return left
    default:
        switch y {
        case 1:
            return up
        case -1:
            return down
        default:
            return zero
        }
    }
}

func (direction Direction) getRight() Direction {
    switch direction {
    case up:
        return right
    case down:
        return left
    case left:
        return up
    case right:
        return down
    }
    panic("Invalid direction")
}

func (direction Direction) getLeft() Direction {
    switch direction {
    case up:
        return left
    case down:
        return right
    case left:
        return down
    case right:
        return up
    }
    panic("Invalid direction")
}

func (direction Direction) getReverse() Direction {
    switch direction {
    case up:
        return down
    case down:
        return up
    case left:
        return right
    case right:
        return left
    }
    panic("Invalid direction")
}

func (direction Direction) String() string {
    return [4]string{"Up", "Down", "Left", "Right"}[direction]
}
