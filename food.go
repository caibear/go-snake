package main

type Food struct {
    tile *Tile
    snake *Snake
}

func newFood(snake *Snake, tile *Tile) *Food {
    food := &Food{
        snake: snake,
        tile: nil,
    }
    food.move(tile.GetPos())
    return food
}

func (food *Food) GetTile() *Tile {
    return food.tile
}

func (food *Food) IsEmpty() bool {
    return true
}

func (food *Food) move(x, y int) (int, int) {
    oldX, oldY := food.tile.GetPos()
    if food.tile != nil && food.tile.owner == food {
        food.tile.setOwner(nil)
    }

    food.tile = food.snake.board.tiles[x][y]
    food.tile.setOwner(food)

    return oldX, oldY
}

func (food *Food) clear() {
    if food.tile != nil {
        if food.tile.owner == food {
            food.tile.setOwner(nil)
        }
        food.tile = nil
    }
}