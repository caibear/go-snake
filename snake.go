package main

const helpingHand = true // Prevent snakes from turning to death

type Snake struct {
    board     *Board
    head      *Segment
    tail      *Segment
    food      *Food
    newLength int
    length    int
}

func newSnake(board *Board, x, y, length int, direction Direction) *Snake {
    if length <= 0 {
        panic("Invalid length")
    }
    newLength := length - 1
    snake := &Snake{board: board, newLength: newLength, length: 1}
    head := newSegment(snake, board.tiles[x][y], direction)
    snake.head = head
    snake.tail = head
    return snake
}

func (snake *Snake) spawnFood() {
    if snake.food == nil {
        snake.food = newFood(snake, snake.board.emptyTile())
    } else {
        x, y := snake.board.emptyPosition()
        snake.food.move(x, y)
    }
}

func (snake *Snake) move(direction Direction) bool {
    var x, y int
    dx, dy := direction.getDelta()
    hx, hy := snake.head.tile.GetPos()
    x, y = AddPosition(hx, hy, dx, dy)

    if !snake.board.IsEmpty(x, y) {
        if helpingHand && direction != snake.GetDirection() {
            direction = snake.GetDirection()
            dx, dy = direction.getDelta()
            x, y = AddPosition(hx, hy, dx, dy)

            if !snake.board.IsEmpty(x, y) {
                return false
            }
        } else {
            return false
        }
    }

    owner := snake.board.tiles[x][y].owner

    if snake.newLength > 0 {
        snake.newLength--
        snake.length++
        s := newSegment(snake, snake.board.tiles[x][y], direction)
        snake.head.previous = s
        s.next = snake.head
        snake.head = s
    } else {
        snake.tail.move(x, y)
        snake.tail.direction = direction
        if snake.head != snake.tail {
            snake.head.previous = snake.tail
            snake.tail.next = snake.head
            snake.head = snake.tail
            snake.tail = snake.tail.previous
            snake.tail.next = nil
        }
    }

    if owner != nil {
        food, isFood := owner.(*Food)
        if isFood {
            food.clear()
            if food.snake == snake {
                snake.newLength += foodLength
            } else {
                snake.newLength += otherFoodLength
            }
        }
    }

    return true
}

func (snake *Snake) shrink() {
    if snake.head != nil {
        if snake.head.next != nil {
            snake.head.clear()
            snake.head = snake.head.next
            snake.head.previous = nil
        } else {
            snake.head.clear()
        }
        snake.length--
    }
    if snake.tail != nil {
        if snake.tail.previous != nil {
            snake.tail.clear()
            snake.tail = snake.tail.previous
            snake.tail.next = nil
        } else {
            snake.tail.clear()
        }
        snake.length--
    }
}

func (snake *Snake) GetDirection() Direction {
    return snake.head.direction
}

func (snake *Snake) GetFood() *Food {
    return snake.food
}

func (snake *Snake) GetPos() (int, int) {
    return snake.head.tile.GetPos()
}

func (snake *Snake) GetHead() *Segment {
    return snake.head
}

func (snake *Snake) GetTail() *Segment {
    return snake.tail
}

func (snake *Snake) GetBoard() *Board {
    return snake.board
}

func (snake *Snake) hasWon() bool {
    return snake.length == boardWidth * boardHeight || board.NumSnakes() == 1
}
