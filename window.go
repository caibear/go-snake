package main

import (
    "fmt"
    "github.com/go-gl/gl/v4.1-core/gl"
    "github.com/go-gl/glfw/v3.2/glfw"
    "log"
    "runtime"
    "strings"
    "sync"
)

const (
    windowName   = "Snake"

    vertexShaderSource = `
    #version 410
    in vec3 vp;
    void main() {
        gl_Position = vec4(vp, 1.0);
    }
    ` + "\x00"  // Null termination character

    fragmentShaderSource = `
    #version 410
    out vec4 frag_colour;
    void main() {
        frag_colour = vec4(1, 1, 1, 1);
    }
    ` + "\x00"  // Null termination character
)

type Window struct {
    win     *glfw.Window // GLFW window
    program uint32       // Shader program

    objects     []Drawable
    objectsLock sync.Mutex
}

func CreateWindow() *Window {
    window := &Window{objects: make([]Drawable, boardWidth*boardHeight)}

    wg.Add(1)
    go window.start()

    return window
}

func (window *Window) start() {
    runtime.LockOSThread()

    window.win = initGlfw()
    window.program = initOpenGL()

    defer glfw.Terminate()
    defer wg.Done()

    for !window.win.ShouldClose() && active {
        window.objectsLock.Lock()
        window.draw()
        window.objectsLock.Unlock()
    }
}

func (window *Window) draw() {
    gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
    gl.UseProgram(window.program)

    frame.Lock()
    for i := 0; i < len(window.objects); i++ {
        object := window.objects[i]
        if object != nil {
            object.getVao().draw()
        }
    }
    frame.Unlock()

    glfw.PollEvents()
    window.win.SwapBuffers()
}

func (window *Window) addDrawable(drawable Drawable) {
    window.objectsLock.Lock()
    window.objects = append(window.objects, drawable)
    window.objectsLock.Unlock()
}

func initGlfw() *glfw.Window {
    if err := glfw.Init(); err != nil {
        panic(err)
    }

    glfw.WindowHint(glfw.ContextVersionMajor, 4)
    glfw.WindowHint(glfw.ContextVersionMinor, 1)
    glfw.WindowHint(glfw.Resizable, glfw.False)
    glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
    glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

    monitor := glfw.GetPrimaryMonitor()
    videoMode := monitor.GetVideoMode()
    win, err := glfw.CreateWindow(videoMode.Width, videoMode.Height, windowName, monitor, nil)
    if err != nil {
        panic(err)
    }

    win.MakeContextCurrent()
    return win
}

func initOpenGL() uint32 {
    if err := gl.Init(); err != nil {
        panic(err)
    }
    version := gl.GoStr(gl.GetString(gl.VERSION))
    log.Println("OpenGL version", version)

    vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
    if err != nil {
        panic(err)
    }

    fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
    if err != nil {
        panic(err)
    }

    program := gl.CreateProgram()
    gl.AttachShader(program, vertexShader)
    gl.AttachShader(program, fragmentShader)
    gl.LinkProgram(program)

    return program
}

func compileShader(source string, shaderType uint32) (uint32, error) {
    shader := gl.CreateShader(shaderType)

    csources, free := gl.Strs(source)
    gl.ShaderSource(shader, 1, csources, nil)
    free()
    gl.CompileShader(shader)

    var status int32
    gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
    if status == gl.FALSE {
        var logLength int32
        gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

        infoLog := strings.Repeat("\x00", int(logLength+1))
        gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(infoLog))

        return 0, fmt.Errorf("failed to compile %v: %v", source, infoLog)
    }

    return shader, nil
}
