package main

import (
    "math/rand"
    "sync"
    "time"
)

const (
    updatesPerSecond = 60
)

var wg sync.WaitGroup
var frame sync.Mutex
var active bool
var window *Window
var board *Board

func main() {
    rand.Seed(time.Now().UnixNano()) // Random seed

    active = true
    window = CreateWindow()
    board = CreateBoard()
    board.AddSnake(newSpaceOperator(), 1)
    go board.Start()
    wg.Wait()
    active = false
}
