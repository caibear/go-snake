package main

import (
    "github.com/beefsack/go-astar"
)

var foundCache [boardWidth][boardHeight]bool
var visualTiles [boardWidth][boardHeight]*Tile

type spaceOperator struct {

}

func initVisual() {
    for i := 0; i < boardWidth; i++ {
        for j := 0; j < boardHeight; j++ {
            visualTiles[i][j] = newVisualTile(i, j)
        }
    }
}

func setVisual(x, y int, visible bool) {
    if !visible {
        visualTiles[x][y].setOwner(nil)
    } else {
        visualTiles[x][y].setOwner(&EmptyOwner{})
    }
}

func newSpaceOperator() *spaceOperator {
    initVisual()
    return &spaceOperator{}
}

func (operator *spaceOperator) GetMove(snake *Snake) Direction {
    path, _, found := astar.Path(snake.GetHead().GetTile(), snake.GetFood().GetTile())
    if found {
        visualize(snake.GetBoard(), path)
    }


    dir := snake.GetDirection()

    directions := [3]Direction{dir, dir.getRight(), dir.getLeft()}
    weights := [3]float64{float64(0), float64(0), float64(0)}

    for i, d := range directions {
        bias := float64(0) // Bias between 0 and 1
        if d == dir {
            bias += 1 // Don't turn bias
        }
        weights[i] = getWeight(snake, d, bias)
    }

    var d Direction
    max := float64(-100)
    for i, w := range weights {
        if w > max {
            max = w
            d = directions[i]
        }
    }

    return d
}

// Weights the move
func getWeight(snake *Snake, d Direction, bias float64) float64 {
    sx, sy := snake.GetPos()
    dx, dy := d.getDelta()
    x, y := AddPosition(sx, sy, dx, dy)

    if !board.IsEmpty(x, y) {
        return -100
    }

    w := bias / ((boardWidth * boardHeight) * 2) // May remove
    //w := float64(0)

    _, distance, found := astar.Path(snake.GetHead().GetTile().GetRelative(d), snake.food.GetTile())
    if found {
        w += 1 / (distance + 1) // Food bias
    }

    b := getBlocks(snake, x, y, d)
    w += float64(b)

    return w
}

func getBlocks(snake *Snake, x, y int, direction Direction) int {
    initCache(snake.board)

    if !foundCache[x][y] {
        blocks := 0

        for _, d := range directionValues {
            if d != direction.getReverse() {
                if b := getNextBlocks(snake, x, y, d); b > blocks {
                    blocks = b
                }
            }
        }

        return blocks
    } else {
        return 0
    }
}

// Amount of usable blocks after move
func getNextBlocks(snake *Snake, x, y int, direction Direction) int {
    initCache(snake.board)
    foundCache[x][y] = true
    dx, dy := direction.getDelta()
    x += dx
    y += dy

    if x >= 0 && y >= 0 && x < boardWidth && y < boardHeight && !foundCache[x][y] {
        b, f := findBlocks(snake, x, y, direction.xParallel())
        if !f {
            return 0
        } else {
            return b
        }
    } else {
        return 0
    }
}

func findBlocks(snake *Snake, x, y int, xPar bool) (int, bool) {
    blocks := 1
    found := false

    foundCache[x][y] = true


    if x + 1 < boardWidth {
        if !foundCache[x + 1][y] {
            b, f := findBlocks(snake, x + 1, y, true)
            blocks += b
            if f {
                found = true
            }
        } else if isTail(snake, snake.GetBoard().Get(x + 1, y)) {
            found = true
        }
    }
    if x - 1 >= 0 {
        if !foundCache[x - 1][y] {
            b, f := findBlocks(snake, x - 1, y, true)
            blocks += b
            if f {
                found = true
            }
        } else if isTail(snake, snake.GetBoard().Get(x - 1, y)) {
            found = true
        }
    }
    if y + 1 < boardHeight {
        if !foundCache[x][y + 1] {
            b, f := findBlocks(snake, x, y + 1, false)
            blocks += b
            if f {
                found = true
            }
        } else if isTail(snake, snake.GetBoard().Get(x, y + 1)) {
            found = true
        }
    }
    if y - 1 >= 0 {
        if !foundCache[x][y - 1] {
            b, f := findBlocks(snake, x, y - 1, false)
            blocks += b
            if f {
                found = true
            }
        } else if isTail(snake, snake.GetBoard().Get(x, y - 1)) {
            found = true
        }
    }

    return blocks, found
}

func isTail(snake *Snake, tile *Tile) bool {
    segment, isSegment := tile.GetOwner().(*Segment)
    if isSegment {
        if segment == snake.tail {
            return true
        }
    }
    return false
}

func initCache(board *Board) {
    for i := 0; i < boardWidth; i++ {
        for j := 0; j < boardHeight; j++ {
            if !board.IsEmpty(i, j) {
                foundCache[i][j] = true
            } else {
                foundCache[i][j] = false
            }
        }
    }
}

func visualize(board *Board, path []astar.Pather) {
    for i := 0; i < boardWidth; i++ {
        for j := 0; j < boardHeight; j++ {
            setVisual(i, j, false)
        }
    }
    for _, p := range path {
        t := p.(*Tile)
        x, y := t.GetPos()
        setVisual(x, y, true)
    }
}