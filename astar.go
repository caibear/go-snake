package main

import "github.com/beefsack/go-astar"

func (tile *Tile) PathNeighbors() []astar.Pather {
    var slice = make([]astar.Pather, 0, 4)

    if tile.EmptyRelative(up) {
        slice = append(slice, tile.GetRelative(up))
    }
    if tile.EmptyRelative(right) {
        slice = append(slice, tile.GetRelative(right))
    }
    if tile.EmptyRelative(down) {
        slice = append(slice, tile.GetRelative(down))
    }
    if tile.EmptyRelative(left) {
        slice = append(slice, tile.GetRelative(left))
    }

    return slice
}

func (tile *Tile) PathNeighborCost(to astar.Pather) float64 {
    return 1
}

func (tile *Tile) PathEstimatedCost(to astar.Pather) float64 {
    return float64(tile.DistanceTo(to.(*Tile)))
}