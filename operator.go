package main

import (
    "math/rand"
)

type Operator interface {
    GetMove(*Snake) Direction
}

type randomOperator struct {
    random *rand.Rand
}

type greedyOperator struct {

}

func newRandomOperator() *randomOperator {
    return &randomOperator{random: rand.New(rand.NewSource(rand.Int63()))}
}

func newGreedyOperator() *greedyOperator {
    return &greedyOperator{}
}

func (randomOperator *randomOperator) GetMove(*Snake) Direction {
    return randomDirection(randomOperator.random)
}

func (greedyOperator *greedyOperator) GetMove(snake *Snake) Direction {
    dir := snake.GetDirection()
    sx, sy := snake.GetPos()
    fx, fy := snake.GetFood().tile.GetPos()

    directions := [3]Direction{dir, dir.getRight(), dir.getLeft()}
    weights := [3]float64{float64(0), float64(0), float64(0)}

    for i, d := range directions {
        dx, dy := d.getDelta()
        x, y := AddPosition(sx, sy, dx, dy)

        dis := Distance2(x, y, fx, fy)
        w := 1 / float64(dis + 1) // Food bias

        if d == dir {
            w += 0.00001 // Turn bias
        }

        if !board.IsEmpty(x, y) {
            w += -100 // Death bias
        }

        weights[i] = w
    }

    var d Direction
    max := float64(0)
    for i, w := range weights {
        if w > max {
            max = w
            d = directions[i]
        }
    }

    return d
}
