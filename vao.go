package main

import (
    "github.com/go-gl/gl/v4.1-core/gl"
)

const floatSize = 4

var triangle = []float32{
    -0.5, 0.5, 0,
    -0.5, -0.5, 0,
    0.5, -0.5, 0,
}

var square = []float32{
    -0.5, 0.5, 0,
    -0.5, -0.5, 0,
    0.5, -0.5, 0,

    -0.5, 0.5, 0,
    0.5, 0.5, 0,
    0.5, -0.5, 0,
}

type Vao struct {
    vertices []float32
    vao      uint32
    visible  bool
}

type Drawable interface {
    getVao() *Vao
}

func createVao(vertices []float32) *Vao {
    return &Vao{vertices: vertices, visible: true}
}

func (object *Vao) init() {
    vertices := object.vertices

    var vbo uint32
    gl.GenBuffers(1, &vbo)
    gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
    gl.BufferData(gl.ARRAY_BUFFER, floatSize*len(vertices), gl.Ptr(vertices), gl.STATIC_DRAW)

    var vao uint32
    gl.GenVertexArrays(1, &vao)
    gl.BindVertexArray(vao)
    gl.EnableVertexAttribArray(0)
    gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
    gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 0, nil)

    object.vao = vao
}

func (object *Vao) draw() {
    if object.visible {
        if object.vao == 0 {
            object.init()
        }

        gl.BindVertexArray(object.vao)
        gl.DrawArrays(gl.TRIANGLES, 0, int32(len(object.vertices)/3))
    }
}
